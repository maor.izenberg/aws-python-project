'''This module handle lambda publishing over SQS'''
import asyncio
import json
import os
from types_aiobotocore_sqs.client import SQSClient
from aiobotocore.session import get_session

async def main():
    '''This handler is scheudled in a constant rate and send a message in SQS'''
    session = get_session()
    async with session.create_client("sqs") as sqs:
        sqs: SQSClient
        message = {'hello': 'from publisher'}
        print(f'Sending a message {message}')
        queue_url=os.getenv('QUEUE_URL','unknown_queue')
        response = await sqs.send_message(QueueUrl=queue_url, MessageBody=json.dumps(message))
        print(f'Response is: {json.dumps(response)}', )

def schedule(event, context):
    '''This handler is scheudled in a constant rate and send a message in SQS'''
    print('Received scheduling event: %s',json.dumps(event))
    asyncio.run(main())
    

