"""This module handle lambda subscripotion of SQS"""
import asyncio
import json
import os
from types_aiobotocore_sqs.client import SQSClient
from aiobotocore.session import get_session


async def main(event):
    """This function handles messages received from the queue"""
    print(f"Received event: {json.dumps(event)}")
    if (
        not event["Records"]
        or not event["Records"][0]
        or not event["Records"][0]["receiptHandle"]
        or not event["Records"][0]["body"]
    ):
        print("message is corrupted")
        return
    session = get_session()
    async with session.create_client("sqs") as sqs:
        sqs: SQSClient
        queue_url = os.getenv("QUEUE_URL", "unknown_queue")
        message = event["Records"][0]
        body = message["body"]
        print(f'received body: {body}')
        receipt_handle = message["receiptHandle"]

        delete_resp = await sqs.delete_message(
            QueueUrl=queue_url, ReceiptHandle=receipt_handle
        )
        print(f"delete_resp: {delete_resp}")


def handle(event, context):
    """This function handles messages received from the queue"""
    asyncio.run(main(event))
